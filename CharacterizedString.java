import java.util.Scanner;
public class CharacterizedString {
    private static Scanner sc;
	public static void main(String[] args) {
		String str;
		sc= new Scanner(System.in);
        System.out.println("========================================");
		System.out.print("Ketik kata/kalimat, lalu tekan enter :  ");
		str = sc.nextLine();
        System.out.println("========================================");
        System.out.println("String Penuh : " + str);
        System.out.println("========================================");
		for(int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			System.out.println("Karakter [" + i + "] : " + ch);
		}
        System.out.println("========================================");
	}
}
